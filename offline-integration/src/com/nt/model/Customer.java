package com.nt.model;

public class Customer {
	
	private Address address;
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Customer [address=" + address + ", email=" + email + ", name=" + name + ", phone=" + phone + "]";
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	private String email;
	private String name;
	private String phone;

	

}
