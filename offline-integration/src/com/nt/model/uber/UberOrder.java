package com.nt.model.uber;

import java.util.List;

public class UberOrder {
	private String username;
	private long order_id;
	private String ordertime;
	private String deleverytime;	
	private List<UberItem> uberitem;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public long getOrder_id() {
		return order_id;
	}
	public void setOrder_id(long order_id) {
		this.order_id = order_id;
	}
	public String getOrdertime() {
		return ordertime;
	}
	public void setOrdertime(String ordertime) {
		this.ordertime = ordertime;
	}
	public String getDeleverytime() {
		return deleverytime;
	}
	public void setDeleverytime(String deleverytime) {
		this.deleverytime = deleverytime;
	}
	public List<UberItem> getUberitem() {
		return uberitem;
	}
	public void setUberitem(List<UberItem> uberitem) {
		this.uberitem = uberitem;
	}
	public Integer getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Integer subtotal) {
		this.subtotal = subtotal;
	}
	public Integer getTax() {
		return tax;
	}
	public void setTax(Integer tax) {
		this.tax = tax;
	}
	public Integer getTotal() {
		return Total;
	}
	public void setTotal(Integer total) {
		Total = total;
	}
	public Integer getDeliveryFees() {
		return deliveryFees;
	}
	public void setDeliveryFees(Integer deliveryFees) {
		this.deliveryFees = deliveryFees;
	}
	
	
	private Integer subtotal;
	private Integer tax;
	private Integer Total;
	private Integer deliveryFees;
	@Override
	public String toString() {
		return "UberOrder [username=" + username + ", order_id=" + order_id + ", ordertime=" + ordertime
				+ ", deleverytime=" + deleverytime + ", uberitem=" + uberitem + ", subtotal=" + subtotal + ", tax="
				+ tax + ", Total=" + Total + ", deliveryFees=" + deliveryFees + "]";
	}
	
	
	
	

}
