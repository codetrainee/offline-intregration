package com.nt.model.uber;

public class UberItem {
	
	private Integer itemquantity;
	private  String itemname;
	private Integer itemprice;	
	private String remarks;
		
	public Integer getItemquantity() {
		return itemquantity;
	}

	public void setItemquantity(Integer itemquantity) {
		this.itemquantity = itemquantity;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public Integer getItemprice() {
		return itemprice;
	}

	public void setItemprice(Integer itemprice) {
		this.itemprice = itemprice;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "UberItem [itemquantity=" + itemquantity + ", itemname=" + itemname + ", itemprice=" + itemprice
				+ ", remarks=" + remarks + "]";
	}


}
