package com.nt.model;

public class Store {
	
	private  String address;
	private float latitude;
	private float longitude;
	private String merchant_ref_id;
	private String  name;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public String getMerchant_ref_id() {
		return merchant_ref_id;
	}
	public void setMerchant_ref_id(String merchant_ref_id) {
		this.merchant_ref_id = merchant_ref_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Store [address=" + address + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", merchant_ref_id=" + merchant_ref_id + ", name=" + name + "]";
	}

	
	
}
