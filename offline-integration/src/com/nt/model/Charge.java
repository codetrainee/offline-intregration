package com.nt.model;

public class Charge {
	
	private String title;
	private int value;
	
	public String getTitle() {
		return title;
	}
	@Override
	public String toString() {
		return "Charge [title=" + title + ", value=" + value + "]";
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}


}
