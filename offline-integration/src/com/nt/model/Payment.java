package com.nt.model;

public class Payment {
	
	private double amount;
	private String option;
  
	
    public double getAmount() {
		return amount;
	}
	@Override
	public String toString() {
		return "Payment [amount=" + amount + ", option=" + option + "]";
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}
	

}
