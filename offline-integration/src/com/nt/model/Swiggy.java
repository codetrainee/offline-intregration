package com.nt.model;

import java.util.List;

public class Swiggy {
	
	
	private List<Restaurant> restaurantTimeMap ;
	private SourceMessageIdMap sourceMessageIdMap;
	
	public List<Restaurant> getRestaurantTimeMap() {
		return restaurantTimeMap;
	}
	public void setRestaurantTimeMap(List<Restaurant> restaurantTimeMap) {
		this.restaurantTimeMap = restaurantTimeMap;
	}
	public SourceMessageIdMap getSourceMessageIdMap() {
		return sourceMessageIdMap;
	}
	public void setSourceMessageIdMap(SourceMessageIdMap sourceMessageIdMap) {
		this.sourceMessageIdMap = sourceMessageIdMap;
	}
	
	
	
	
	@Override
	public String toString() {
		return "Swiggy [restaurantTimeMap=" + restaurantTimeMap + ", sourceMessageIdMap=" + sourceMessageIdMap + "]";
	}
	
	
	

}
