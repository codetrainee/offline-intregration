package com.nt.model;

public class SourceMessageIdMap {
	
	
	private String  source;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return "Customer [source=" + source + "]";
	}

}
