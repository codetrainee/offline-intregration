package com.nt.model;

public class OptionToAdd {

	public String getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public String toString() {
		return "OptionToAdd [merchant_id=" + merchant_id + ", price=" + price + ", title=" + title + "]";
	}
	private String merchant_id;
	private int price;
	private String title;

}
