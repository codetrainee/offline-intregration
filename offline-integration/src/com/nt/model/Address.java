package com.nt.model;

public class Address {
	private String city;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Boolean getIs_guest_mode() {
		return is_guest_mode;
	}
	public void setIs_guest_mode(Boolean is_guest_mode) {
		this.is_guest_mode = is_guest_mode;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLine_1() {
		return line_1;
	}
	public void setLine_1(String line_1) {
		this.line_1 = line_1;
	}
	public String getLine_2() {
		return line_2;
	}
	public void setLine_2(String line_2) {
		this.line_2 = line_2;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getSub_locality() {
		return sub_locality;
	}
	public void setSub_locality(String sub_localityl) {
		this.sub_locality = sub_localityl;
	}
	private Boolean is_guest_mode;
	private String latitude;
	@Override
	public String toString() {
		return "Address [city=" + city + ", is_guest_mode=" + is_guest_mode + ", latitude=" + latitude + ", line_1="
				+ line_1 + ", line_2=" + line_2 + ", longitude=" + longitude + ", pin=" + pin + ", sub_locality="
				+ sub_locality + "]";
	}
	private String line_1;
	private String line_2;
	private String longitude;
	private String pin;
	private String sub_locality;
	

}
