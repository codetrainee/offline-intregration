package com.nt.model;

import java.util.List;

public class Extra_platform {
	private String delivery_type;

	private List<Discount> discounts;	
	private String id;
	private String kind;
	private String name;
	
	public String getDelivery_type() {
		return delivery_type;
	}

	public void setDelivery_type(String delivery_type) {
		this.delivery_type = delivery_type;
	}

	

	public List<Discount> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	@Override
	public String toString() {
		return "Extra_platform [delivery_type=" + delivery_type + ", discounts=" + discounts + ", id=" + id + ", kind="
				+ kind + ", name=" + name + "]";
	}
	

}
