package com.nt.model;

import java.util.Date;
import java.util.List;

public class DeleveryStatus {

private String comments;
private int created;
private String status;

public String getComments() {
	return comments;
}
public void setComments(String comments) {
	this.comments = comments;
}
public int getCreated() {
	return created;
}
public void setCreated(int created) {
	this.created = created;
}

public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}



@Override
public String toString() {
	return "DeleveryStatus [comments=" + comments + ", created=" + created + ", status=" + status + "]";
}


	

	
	

}
