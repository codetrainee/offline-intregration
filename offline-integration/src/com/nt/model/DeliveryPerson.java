package com.nt.model;

public class DeliveryPerson {
	
	private String alt_phone;
	private String name;
	private String phone;
	
	public String getAlt_phone() {
		return alt_phone;
	}
	public void setAlt_phone(String alt_phone) {
		this.alt_phone = alt_phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Override
	public String toString() {
		return "DeliveryPerson [alt_phone=" + alt_phone + ", name=" + name + ", phone=" + phone + "]";
	}
	
	
	

}
