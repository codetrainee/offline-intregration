package com.nt.model;

import java.util.List;

public class Delevery {
	
	private int order_id;
    private String status;
	
	private  DeliveryPerson deliveryPerson;
	
	private List<DeleveryStatus> deliveryStatus;
	
	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DeliveryPerson getDeliveryPerson() {
		return deliveryPerson;
	}

	public void setDeliveryPerson(DeliveryPerson deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}

	public List<DeleveryStatus> getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(List<DeleveryStatus> deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	
	
	@Override
	public String toString() {
		return "Delevery [order_id=" + order_id + ", status=" + status + ", deliveryPerson=" + deliveryPerson
				+ ", deliveryStatus=" + deliveryStatus + "]";
	}


}
