package com.nt.model;

import java.util.List;

public class Order {
	
	
	@Override
	public String toString() {
		return "Order [details=" + details + ", items=" + items + ", next_state=" + next_state + ", next_states="
				+ next_states + ", payment=" + payment + ", store=" + store + "]";
	}
	
	
	public Details details;
	public List<Item> items;
	public  String next_state;
	public List<String> next_states;
	public List<Payment> payment;
	public Store store;

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public String getNext_state() {
		return next_state;
	}

	public void setNext_state(String next_state) {
		this.next_state = next_state;
	}

	public List<Payment> getPayment() {
		return payment;
	}

	public void setPayment(List<Payment> payment) {
		this.payment = payment;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public List<String> getNext_states() {
		return next_states;
	}

	public void setNext_states(List<String> next_states) {
		this.next_states = next_states;
	}


}
