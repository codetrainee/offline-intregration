package com.nt.model;

public class ItemTaxes {
	private float rate;
	private String title;
	private float value;
	
	@Override
	public String toString() {
		return "ItemTaxes [rate=" + rate + ", title=" + title + ", value=" + value + "]";
	}
	public float getRate() {
		return rate;
	}
	public void setRate(float rate) {
		this.rate = rate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}


}
