package com.nt.model;

import java.util.List;
import java.util.Map;

public class Details {
	private String channel;
	private List<Charge> charges;	
	private String coupon;	
	
	private int created;
	private String delivery_datetime;
	private float discount;
	private List<Extra_platform> ext_platforms;

	private String instructions;
	private Integer item_level_total_charges;
	private float item_level_total_taxes;
	
	private int item_taxes;
	
	private int order_level_total_charges;
	private int order_level_total_taxes;
	private String order_state;
	
	
	private int order_subtotal;
	
	private float order_total;
	private String order_type;	
	
	private String state;	
	private List<String> taxes;
	private int total_charges;	
	private int total_external_discount;
	private float total_taxes;
	
	
	

	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public List<Charge> getCharges() {
		return charges;
	}
	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}
	public String getCoupon() {
		return coupon;
	}
	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}
	public int getCreated() {
		return created;
	}
	public void setCreated(int created) {
		this.created = created;
	}
	public String getDelivery_datetime() {
		return delivery_datetime;
	}
	public void setDelivery_datetime(String delivery_datetime) {
		this.delivery_datetime = delivery_datetime;
	}
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(float discount) {
		this.discount = discount;
	}
	public List<Extra_platform> getExt_platforms() {
		return ext_platforms;
	}
	public void setExt_platforms(List<Extra_platform> ext_platforms) {
		this.ext_platforms = ext_platforms;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public Integer getItem_level_total_charges() {
		return item_level_total_charges;
	}
	public void setItem_level_total_charges(Integer item_level_total_charges) {
		this.item_level_total_charges = item_level_total_charges;
	}
	public float getItem_level_total_taxes() {
		return item_level_total_taxes;
	}
	public void setItem_level_total_taxes(float item_level_total_taxes) {
		this.item_level_total_taxes = item_level_total_taxes;
	}
	public int getItem_taxes() {
		return item_taxes;
	}
	public void setItem_taxes(int item_taxes) {
		this.item_taxes = item_taxes;
	}
	public int getOrder_level_total_charges() {
		return order_level_total_charges;
	}
	public void setOrder_level_total_charges(int order_level_total_charges) {
		this.order_level_total_charges = order_level_total_charges;
	}
	public int getOrder_level_total_taxes() {
		return order_level_total_taxes;
	}
	public void setOrder_level_total_taxes(int order_level_total_taxes) {
		this.order_level_total_taxes = order_level_total_taxes;
	}
	public String getOrder_state() {
		return order_state;
	}
	public void setOrder_state(String order_state) {
		this.order_state = order_state;
	}
	public int getOrder_subtotal() {
		return order_subtotal;
	}
	public void setOrder_subtotal(int order_subtotal) {
		this.order_subtotal = order_subtotal;
	}
	public float getOrder_total() {
		return order_total;
	}
	public void setOrder_total(float order_total) {
		this.order_total = order_total;
	}
	public String getOrder_type() {
		return order_type;
	}
	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getTotal_charges() {
		return total_charges;
	}
	public void setTotal_charges(int total_charges) {
		this.total_charges = total_charges;
	}
	public int getTotal_external_discount() {
		return total_external_discount;
	}
	public void setTotal_external_discount(int total_external_discount) {
		this.total_external_discount = total_external_discount;
	}
	public float getTotal_taxes() {
		return total_taxes;
	}
	public void setTotal_taxes(float total_taxes) {
		this.total_taxes = total_taxes;
	}
	public List<String> getTaxes() {
		return taxes;
	}
	public void setTaxes(List<String> taxes) {
		this.taxes = taxes;
	}

	@Override
	public String toString() {
		return "Details [channel=" + channel + ", charges=" + charges + ", coupon=" + coupon + ", created=" + created
				+ ", delivery_datetime=" + delivery_datetime + ", discount=" + discount + ", ext_platforms="
				+ ext_platforms + ", instructions=" + instructions + ", item_level_total_charges="
				+ item_level_total_charges + ", item_level_total_taxes=" + item_level_total_taxes + ", item_taxes="
				+ item_taxes + ", order_level_total_charges=" + order_level_total_charges + ", order_level_total_taxes="
				+ order_level_total_taxes + ", order_state=" + order_state + ", order_subtotal=" + order_subtotal
				+ ", order_total=" + order_total + ", order_type=" + order_type + ", state=" + state + ", taxes="
				+ taxes + ", total_charges=" + total_charges + ", total_external_discount=" + total_external_discount
				+ ", total_taxes=" + total_taxes + "]";
	}	

}
