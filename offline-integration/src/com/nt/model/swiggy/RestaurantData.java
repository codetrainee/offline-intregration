package com.nt.model.swiggy;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestaurantData {
	private Integer restaurantId;
	private String lastUpdatedTime;
	private String serverTime;
	private Boolean isOpen;
	private long lastCachePollTime;	
	private  Batches  batches;
	private List<String> updatedOrderIds;
	private List<String> ordersToBeDeleted;
	private List<Orders>  orders;
	private List<String> popOrders;
	
	
	public Integer getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(String lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public String getServerTime() {
		return serverTime;
	}
	public void setServerTime(String serverTime) {
		this.serverTime = serverTime;
	}
	public Boolean getIsOpen() {
		return isOpen;
	}
	public void setIsOpen(Boolean isOpen) {
		this.isOpen = isOpen;
	}
	public long getLastCachePollTime() {
		return lastCachePollTime;
	}
	public void setLastCachePollTime(long lastCachePollTime) {
		this.lastCachePollTime = lastCachePollTime;
	}
	public Batches getBatches() {
		return batches;
	}
	public void setBatches(Batches batches) {
		this.batches = batches;
	}
	public List<String> getUpdatedOrderIds() {
		return updatedOrderIds;
	}
	public void setUpdatedOrderIds(List<String> updatedOrderIds) {
		this.updatedOrderIds = updatedOrderIds;
	}
	public List<String> getOrdersToBeDeleted() {
		return ordersToBeDeleted;
	}
	public void setOrdersToBeDeleted(List<String> ordersToBeDeleted) {
		this.ordersToBeDeleted = ordersToBeDeleted;
	}
	public List<Orders> getOrders() {
		return orders;
	}
	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}
	public List<String> getPopOrders() {
		return popOrders;
	}
	public void setPopOrders(List<String> popOrders) {
		this.popOrders = popOrders;
	}
	@Override
	public String toString() {
		return "RestaurantData [restaurantId=" + restaurantId + ", lastUpdatedTime=" + lastUpdatedTime + ", serverTime="
				+ serverTime + ", isOpen=" + isOpen + ", lastCachePollTime=" + lastCachePollTime + ", batches="
				+ batches + ", updatedOrderIds=" + updatedOrderIds + ", ordersToBeDeleted=" + ordersToBeDeleted
				+ ", orders=" + orders + ", popOrders=" + popOrders + "]";
	}
	


}
