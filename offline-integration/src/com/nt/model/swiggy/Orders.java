package com.nt.model.swiggy;

import java.util.List;

public class Orders {
	private String last_updated_time;
	private String order_id;
	private Status status;
	private String current_order_action;
	private DeleveryBoy delivery_boy;
	private String customer_comment;
	private String customer_area;
	public String getLast_updated_time() {
		return last_updated_time;
	}
	public void setLast_updated_time(String last_updated_time) {
		this.last_updated_time = last_updated_time;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getCurrent_order_action() {
		return current_order_action;
	}
	public void setCurrent_order_action(String current_order_action) {
		this.current_order_action = current_order_action;
	}
	public DeleveryBoy getDelivery_boy() {
		return delivery_boy;
	}
	public void setDelivery_boy(DeleveryBoy delivery_boy) {
		this.delivery_boy = delivery_boy;
	}
	public String getCustomer_comment() {
		return customer_comment;
	}
	public void setCustomer_comment(String customer_comment) {
		this.customer_comment = customer_comment;
	}
	public String getCustomer_area() {
		return customer_area;
	}
	public void setCustomer_area(String customer_area) {
		this.customer_area = customer_area;
	}
	public float getCustomer_distance() {
		return customer_distance;
	}
	public void setCustomer_distance(float customer_distance) {
		this.customer_distance = customer_distance;
	}
	public Restaurants getRestaurant_details() {
		return restaurant_details;
	}
	public void setRestaurant_details(Restaurants restaurant_details) {
		this.restaurant_details = restaurant_details;
	}
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public Gst_details getGST_details() {
		return GST_details;
	}
	public void setGST_details(Gst_details gST_details) {
		GST_details = gST_details;
	}
	public Integer getGst() {
		return gst;
	}
	public void setGst(Integer gst) {
		this.gst = gst;
	}
	public Integer getServiceCharge() {
		return serviceCharge;
	}
	public void setServiceCharge(Integer serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public Integer getSpending() {
		return spending;
	}
	public void setSpending(Integer spending) {
		this.spending = spending;
	}
	public Integer getTax() {
		return tax;
	}
	public void setTax(Integer tax) {
		this.tax = tax;
	}
	public Integer getDiscount() {
		return discount;
	}
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}
	public Integer getBill() {
		return bill;
	}
	public void setBill(Integer bill) {
		this.bill = bill;
	}
	public Integer getRestaurant_trade_discount() {
		return restaurant_trade_discount;
	}
	public void setRestaurant_trade_discount(Integer restaurant_trade_discount) {
		this.restaurant_trade_discount = restaurant_trade_discount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Cage_data getCafe_data() {
		return cafe_data;
	}
	public void setCafe_data(Cage_data cafe_data) {
		this.cafe_data = cafe_data;
	}
	public Boolean getIs_assured() {
		return is_assured;
	}
	public void setIs_assured(Boolean is_assured) {
		this.is_assured = is_assured;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Boolean getIsMFRAccuracyCalculated() {
		return isMFRAccuracyCalculated;
	}
	public void setIsMFRAccuracyCalculated(Boolean isMFRAccuracyCalculated) {
		this.isMFRAccuracyCalculated = isMFRAccuracyCalculated;
	}
	public String getFoodHandoverTimeSec() {
		return foodHandoverTimeSec;
	}
	public void setFoodHandoverTimeSec(String foodHandoverTimeSec) {
		this.foodHandoverTimeSec = foodHandoverTimeSec;
	}
	private float customer_distance;
	private Restaurants  restaurant_details;
	private Cart cart;
	
	private  Gst_details GST_details;
	
	private Integer gst;
	private Integer serviceCharge;
	private Integer spending;
	private Integer tax;
	private Integer discount;
	private Integer bill;
	
	private Integer restaurant_trade_discount;
	
	private String type;
	private Cage_data cafe_data;
	
	private Boolean is_assured;
	
	private   Customer customer;
	private Boolean isMFRAccuracyCalculated;
	private String foodHandoverTimeSec;
	
	
	@Override
	public String toString() {
		return "Orders [last_updated_time=" + last_updated_time + ", order_id=" + order_id + ", status=" + status
				+ ", current_order_action=" + current_order_action + ", delivery_boy=" + delivery_boy
				+ ", customer_comment=" + customer_comment + ", customer_area=" + customer_area + ", customer_distance="
				+ customer_distance + ", restaurant_details=" + restaurant_details + ", cart=" + cart + ", GST_details="
				+ GST_details + ", gst=" + gst + ", serviceCharge=" + serviceCharge + ", spending=" + spending
				+ ", tax=" + tax + ", discount=" + discount + ", bill=" + bill + ", restaurant_trade_discount="
				+ restaurant_trade_discount + ", type=" + type + ", cafe_data=" + cafe_data + ", is_assured="
				+ is_assured + ", customer=" + customer + ", isMFRAccuracyCalculated=" + isMFRAccuracyCalculated
				+ ", foodHandoverTimeSec=" + foodHandoverTimeSec + "]";
	}
	

	

}
