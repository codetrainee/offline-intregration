package com.nt.model.swiggy;

public class Gst_details {
	private Integer cartCGST;
	private Integer cartIGST;
	private Integer cartSGST;
	private Integer itemCGST;
	private Integer itemIGST;
	private Integer packagingCGST;
	private Integer packagingSGST;
	public Integer getCartCGST() {
		return cartCGST;
	}
	public void setCartCGST(Integer cartCGST) {
		this.cartCGST = cartCGST;
	}
	public Integer getCartIGST() {
		return cartIGST;
	}
	public void setCartIGST(Integer cartIGST) {
		this.cartIGST = cartIGST;
	}
	public Integer getCartSGST() {
		return cartSGST;
	}
	public void setCartSGST(Integer cartSGST) {
		this.cartSGST = cartSGST;
	}
	public Integer getItemCGST() {
		return itemCGST;
	}
	public void setItemCGST(Integer itemCGST) {
		this.itemCGST = itemCGST;
	}
	public Integer getItemIGST() {
		return itemIGST;
	}
	public void setItemIGST(Integer itemIGST) {
		this.itemIGST = itemIGST;
	}
	public Integer getPackagingCGST() {
		return packagingCGST;
	}
	public void setPackagingCGST(Integer packagingCGST) {
		this.packagingCGST = packagingCGST;
	}
	public Integer getPackagingSGST() {
		return packagingSGST;
	}
	public void setPackagingSGST(Integer packagingSGST) {
		this.packagingSGST = packagingSGST;
	}
	@Override
	public String toString() {
		return "Gst_details [cartCGST=" + cartCGST + ", cartIGST=" + cartIGST + ", cartSGST=" + cartSGST + ", itemCGST="
				+ itemCGST + ", itemIGST=" + itemIGST + ", packagingCGST=" + packagingCGST + ", packagingSGST="
				+ packagingSGST + "]";
	}
	
	
	

}
