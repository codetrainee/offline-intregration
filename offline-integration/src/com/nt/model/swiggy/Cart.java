package com.nt.model.swiggy;

import java.util.List;

public class Cart {
	private  Charges charges; 
	private  List<SwiggyItem> items;
	

	public Charges getCharges() {
		return charges;
	}
	public void setCharges(Charges charges) {
		this.charges = charges;
	}
	public List<SwiggyItem> getItems() {
		return items;
	}
	public void setItems(List<SwiggyItem> items) {
		this.items = items;
	}
	@Override
	public String toString() {
		return "Cart [charges=" + charges + ", items=" + items + "]";
	}
	
	
	
}
