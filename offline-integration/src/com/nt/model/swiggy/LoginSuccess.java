package com.nt.model.swiggy;

import java.util.List;

public class LoginSuccess {
	private Integer statusCode;
	private String statusMessage;
	private Integer user_id;
	private List<Integer> permissions;
	private List<Outlets> outlets;
	private Integer role_id;
	private String redirect_url;
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public List<Integer> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Integer> permissions) {
		this.permissions = permissions;
	}
	public List<Outlets> getOutlets() {
		return outlets;
	}
	public void setOutlets(List<Outlets> outlets) {
		this.outlets = outlets;
	}
	public Integer getRole_id() {
		return role_id;
	}
	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}
	public String getRedirect_url() {
		return redirect_url;
	}
	public void setRedirect_url(String redirect_url) {
		this.redirect_url = redirect_url;
	}
	
	
	
	@Override
	public String toString() {
		return "LoginSuccess [statusCode=" + statusCode + ", statusMessage=" + statusMessage + ", user_id=" + user_id
				+ ", permissions=" + permissions + ", outlets=" + outlets + ", role_id=" + role_id + ", redirect_url="
				+ redirect_url + "]";
	}
	
	
	
	


}
