package com.nt.model.swiggy;

public class Status {
	private String order_status;
	private String placed_status;
	private String placingState;
	private String delivery_status;
	private String placed_time;
	private String assigned_time;
	private String call_partner_time;
	private String ordered_time;
	private String edited_status;
	private String edited_time;
	private String food_prep_time;
	private String cancelled_time;
	private Boolean trueArrived;
	private String arrived_time;
	public String getOrder_status() {
		return order_status;
	}
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	public String getPlaced_status() {
		return placed_status;
	}
	public void setPlaced_status(String placed_status) {
		this.placed_status = placed_status;
	}
	public String getPlacingState() {
		return placingState;
	}
	public void setPlacingState(String placingState) {
		this.placingState = placingState;
	}
	public String getDelivery_status() {
		return delivery_status;
	}
	public void setDelivery_status(String delivery_status) {
		this.delivery_status = delivery_status;
	}
	public String getPlaced_time() {
		return placed_time;
	}
	public void setPlaced_time(String placed_time) {
		this.placed_time = placed_time;
	}
	public String getAssigned_time() {
		return assigned_time;
	}
	public void setAssigned_time(String assigned_time) {
		this.assigned_time = assigned_time;
	}
	public String getCall_partner_time() {
		return call_partner_time;
	}
	public void setCall_partner_time(String call_partner_time) {
		this.call_partner_time = call_partner_time;
	}
	public String getOrdered_time() {
		return ordered_time;
	}
	public void setOrdered_time(String ordered_time) {
		this.ordered_time = ordered_time;
	}
	public String getEdited_status() {
		return edited_status;
	}
	public void setEdited_status(String edited_status) {
		this.edited_status = edited_status;
	}
	public String getEdited_time() {
		return edited_time;
	}
	public void setEdited_time(String edited_time) {
		this.edited_time = edited_time;
	}
	public String getFood_prep_time() {
		return food_prep_time;
	}
	public void setFood_prep_time(String food_prep_time) {
		this.food_prep_time = food_prep_time;
	}
	public String getCancelled_time() {
		return cancelled_time;
	}
	public void setCancelled_time(String cancelled_time) {
		this.cancelled_time = cancelled_time;
	}
	public Boolean getTrueArrived() {
		return trueArrived;
	}
	public void setTrueArrived(Boolean trueArrived) {
		this.trueArrived = trueArrived;
	}
	public String getArrived_time() {
		return arrived_time;
	}
	public void setArrived_time(String arrived_time) {
		this.arrived_time = arrived_time;
	}
	@Override
	public String toString() {
		return "Status [order_status=" + order_status + ", placed_status=" + placed_status + ", placingState="
				+ placingState + ", delivery_status=" + delivery_status + ", placed_time=" + placed_time
				+ ", assigned_time=" + assigned_time + ", call_partner_time=" + call_partner_time + ", ordered_time="
				+ ordered_time + ", edited_status=" + edited_status + ", edited_time=" + edited_time
				+ ", food_prep_time=" + food_prep_time + ", cancelled_time=" + cancelled_time + ", trueArrived="
				+ trueArrived + ", arrived_time=" + arrived_time + "]";
	}
	

	

}
