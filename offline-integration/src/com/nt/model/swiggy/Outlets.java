package com.nt.model.swiggy;

public class Outlets {
	private Integer rest_rid;
	private String rest_name;
	private Integer area_id;
	private String rest_area;
	private Integer rest_city_id;
	private String rest_city;
	private String rest_locality;
	private String tier;
	private double rating;
	private boolean assured;
	
	
	public Integer getRest_rid() {
		return rest_rid;
	}
	public void setRest_rid(Integer rest_rid) {
		this.rest_rid = rest_rid;
	}
	public String getRest_name() {
		return rest_name;
	}
	public void setRest_name(String rest_name) {
		this.rest_name = rest_name;
	}
	public Integer getArea_id() {
		return area_id;
	}
	public void setArea_id(Integer area_id) {
		this.area_id = area_id;
	}
	public String getRest_area() {
		return rest_area;
	}
	public void setRest_area(String rest_area) {
		this.rest_area = rest_area;
	}
	public Integer getRest_city_id() {
		return rest_city_id;
	}
	public void setRest_city_id(Integer rest_city_id) {
		this.rest_city_id = rest_city_id;
	}
	public String getRest_city() {
		return rest_city;
	}
	public void setRest_city(String rest_city) {
		this.rest_city = rest_city;
	}
	public String getRest_locality() {
		return rest_locality;
	}
	public void setRest_locality(String rest_locality) {
		this.rest_locality = rest_locality;
	}
	public String getTier() {
		return tier;
	}
	public void setTier(String tier) {
		this.tier = tier;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public boolean isAssured() {
		return assured;
	}
	public void setAssured(boolean assured) {
		this.assured = assured;
	}
	@Override
	public String toString() {
		return "Outlets [rest_rid=" + rest_rid + ", rest_name=" + rest_name + ", area_id=" + area_id + ", rest_area="
				+ rest_area + ", rest_city_id=" + rest_city_id + ", rest_city=" + rest_city + ", rest_locality="
				+ rest_locality + ", tier=" + tier + ", rating=" + rating + ", assured=" + assured + "]";
	}
	

}
