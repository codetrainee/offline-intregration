package com.nt.model.swiggy;

import java.util.List;

public class SwiggyItem {
	private String item_id;
	private Integer quantity;
	private String name;
	private Integer restaurant_discount_hit;
	private Integer sub_total;
	private Integer total;
	private String category;
	private String sub_category;
	private ItemCharge charges;
	private tax_expression tax_expressions;
	
	private List<String> addons;
	private String variants;
	
	private List<String> newAddons;
	private List<String> newVariants;
	private Boolean is_oos;
	private String is_veg;
	private Boolean reward_type;
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRestaurant_discount_hit() {
		return restaurant_discount_hit;
	}
	public void setRestaurant_discount_hit(Integer restaurant_discount_hit) {
		this.restaurant_discount_hit = restaurant_discount_hit;
	}
	public Integer getSub_total() {
		return sub_total;
	}
	public void setSub_total(Integer sub_total) {
		this.sub_total = sub_total;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSub_category() {
		return sub_category;
	}
	public void setSub_category(String sub_category) {
		this.sub_category = sub_category;
	}
	public ItemCharge getCharges() {
		return charges;
	}
	public void setCharges(ItemCharge charges) {
		this.charges = charges;
	}
	public tax_expression getTax_expressions() {
		return tax_expressions;
	}
	public void setTax_expressions(tax_expression tax_expressions) {
		this.tax_expressions = tax_expressions;
	}
	public List<String> getAddons() {
		return addons;
	}
	public void setAddons(List<String> addons) {
		this.addons = addons;
	}
	public String getVariants() {
		return variants;
	}
	public void setVariants(String variants) {
		this.variants = variants;
	}
	public List<String> getNewAddons() {
		return newAddons;
	}
	public void setNewAddons(List<String> newAddons) {
		this.newAddons = newAddons;
	}
	public List<String> getNewVariants() {
		return newVariants;
	}
	public void setNewVariants(List<String> newVariants) {
		this.newVariants = newVariants;
	}
	public Boolean getIs_oos() {
		return is_oos;
	}
	public void setIs_oos(Boolean is_oos) {
		this.is_oos = is_oos;
	}
	public String getIs_veg() {
		return is_veg;
	}
	public void setIs_veg(String is_veg) {
		this.is_veg = is_veg;
	}
	public Boolean getReward_type() {
		return reward_type;
	}
	public void setReward_type(Boolean reward_type) {
		this.reward_type = reward_type;
	}
	@Override
	public String toString() {
		return "Item [item_id=" + item_id + ", quantity=" + quantity + ", name=" + name + ", restaurant_discount_hit="
				+ restaurant_discount_hit + ", sub_total=" + sub_total + ", total=" + total + ", category=" + category
				+ ", sub_category=" + sub_category + ", charges=" + charges + ", tax_expressions=" + tax_expressions
				+ ", addons=" + addons + ", variants=" + variants + ", newAddons=" + newAddons + ", newVariants="
				+ newVariants + ", is_oos=" + is_oos + ", is_veg=" + is_veg + ", reward_type=" + reward_type + "]";
	}
	

}
