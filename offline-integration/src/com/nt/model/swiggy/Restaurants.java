package com.nt.model.swiggy;

public class Restaurants {
	private String restaurant_lat;
	private String restaurant_lng;
	public String getRestaurant_lat() {
		return restaurant_lat;
	}
	public void setRestaurant_lat(String restaurant_lat) {
		this.restaurant_lat = restaurant_lat;
	}
	public String getRestaurant_lng() {
		return restaurant_lng;
	}
	public void setRestaurant_lng(String restaurant_lng) {
		this.restaurant_lng = restaurant_lng;
	}
	@Override
	public String toString() {
		return "Restaurants [restaurant_lat=" + restaurant_lat + ", restaurant_lng=" + restaurant_lng + "]";
	}
	
	
	

}
