package com.nt.model.swiggy;

public class DeleveryBoy {
	private String name;
	private String mobile;
	private String imageUrl;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	@Override
	public String toString() {
		return "DeleveryBoy [name=" + name + ", mobile=" + mobile + ", imageUrl=" + imageUrl + "]";
	}
	
	
	

}
