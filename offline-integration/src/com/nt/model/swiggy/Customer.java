package com.nt.model.swiggy;

public class Customer {
	
	private String customer_id;
	private String customer_lat;
	private String customer_lng;
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_lat() {
		return customer_lat;
	}
	public void setCustomer_lat(String customer_lat) {
		this.customer_lat = customer_lat;
	}
	public String getCustomer_lng() {
		return customer_lng;
	}
	public void setCustomer_lng(String customer_lng) {
		this.customer_lng = customer_lng;
	}
	@Override
	public String toString() {
		return "Customer [customer_id=" + customer_id + ", customer_lat=" + customer_lat + ", customer_lng="
				+ customer_lng + "]";
	}
	
	
	

}
