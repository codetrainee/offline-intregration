package com.nt.model.swiggy;

public class Cage_data {
	
	private Integer restaurant_type;

	public Integer getRestaurant_type() {
		return restaurant_type;
	}

	public void setRestaurant_type(Integer restaurant_type) {
		this.restaurant_type = restaurant_type;
	}

	@Override
	public String toString() {
		return "Cage_data [restaurant_type=" + restaurant_type + "]";
	}

}
