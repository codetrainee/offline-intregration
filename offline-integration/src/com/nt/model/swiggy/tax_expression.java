package com.nt.model.swiggy;

public class tax_expression {
	private Boolean GST_inclusive;
	private String Service_Charges;
	private String Vat;
	private String Service_Tax;
	public Boolean getGST_inclusive() {
		return GST_inclusive;
	}
	public void setGST_inclusive(Boolean gST_inclusive) {
		GST_inclusive = gST_inclusive;
	}
	public String getService_Charges() {
		return Service_Charges;
	}
	public void setService_Charges(String service_Charges) {
		Service_Charges = service_Charges;
	}
	public String getVat() {
		return Vat;
	}
	public void setVat(String vat) {
		Vat = vat;
	}
	public String getService_Tax() {
		return Service_Tax;
	}
	public void setService_Tax(String service_Tax) {
		Service_Tax = service_Tax;
	}
	@Override
	public String toString() {
		return "tax_expression [GST_inclusive=" + GST_inclusive + ", Service_Charges=" + Service_Charges + ", Vat="
				+ Vat + ", Service_Tax=" + Service_Tax + "]";
	}
	
	
	
	
	

}
