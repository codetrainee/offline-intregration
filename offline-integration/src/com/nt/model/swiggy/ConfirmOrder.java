package com.nt.model.swiggy;

public class ConfirmOrder {
  public long getOrder_id() {
		return order_id;
	}
	public void setOrder_id(long order_id) {
		this.order_id = order_id;
	}
	public long getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}
	public Integer getPrep_time() {
		return prep_time;
	}
	public void setPrep_time(Integer prep_time) {
		this.prep_time = prep_time;
	}
	
   private long order_id;
   private long restaurantId;
   private Integer  prep_time;
   
   @Override
   public String toString() {
	return "ConfirmOrder [order_id=" + order_id + ", restaurantId=" + restaurantId + ", prep_time=" + prep_time + "]";
}
  
	
	
	
	

}
