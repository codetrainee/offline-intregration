package com.uber.test;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;

import com.nt.model.Charge;
import com.nt.model.Delevery;
import com.nt.model.Details;
import com.nt.model.Item;
import com.nt.model.Uber;
import com.nt.model.uber.UberItem;
import com.nt.model.uber.UberOrder;
import con.xyz.uber.UberThread;
public class WriteData {
	
	static String line;
	static BufferedReader br;
	static ArrayList listitemdata = new ArrayList();
	static ArrayList remarks = new ArrayList();
	static ArrayList listurl = new ArrayList();
	static ArrayList deleveryboy = new ArrayList();
	static ArrayList listprice = new ArrayList();
	
	
	 static	int index;
	 Uber uber = new Uber();
	
	 List<Delevery> deleviers = new ArrayList<Delevery>();	  
	 UberItem uberitem =null;
	 Delevery delevery = null;
	 UberOrder uberorder = new UberOrder();
	 
	 
	Details details = null;
	Item itemdata =null;
	
	 public void writedata(String html) {	
		
		try {

			Document doc = Jsoup.parse(html);
			Elements memberelements = doc.select("div[class*=rn-1awozwy rn-1efd50x rn-14skgim rn-rull8r rn-mm0ijv rn-13yce4e rn-fnigne rn-ndvcnb rn-gxnn5r rn-deolkf rn-1loqt21 rn-6koalj rn-13awgt0 rn-1m1wadx rn-1awmn5t rn-18u37iz rn-1777fci rn-1mnahxq rn-61z16t rn-p1pxzi rn-11wrixw rn-ifefl9 rn-bcqeeo rn-wk8lta rn-9aemit rn-1mdbw0j rn-gy4na3 rn-bnwqim rn-1otgn73 rn-1lgpqti]");					
			Elements membere = memberelements.select("div[dir*=auto]");
			for (Element elements : membere ) {
				String recordsdata = elements.text();
				listurl.add(recordsdata);

			}
			
			String username = (String) listurl.get(0);
			long orderid = (long) listurl.get(1);
			String ordertime = (String) listurl.get(2);
			String deleverytime = (String) listurl.get(3);
						
			uberorder.setUsername(username);
	        uberorder.setOrder_id(orderid);	
	        uberorder.setOrdertime(ordertime);
	        uberorder.setDeleverytime(deleverytime);

			
	        Elements els=doc.select("div[class*=rn-1awozwy rn-1efd50x rn-14skgim rn-rull8r rn-mm0ijv rn-13yce4e rn-fnigne rn-ndvcnb rn-gxnn5r rn-deolkf rn-6koalj rn-1pxmb3b rn-7vfszb rn-18u37iz rn-1mnahxq rn-61z16t rn-p1pxzi rn-11wrixw rn-ifefl9 rn-bcqeeo rn-wk8lta rn-9aemit rn-1mdbw0j rn-gy4na3 rn-bnwqim rn-1lgpqti]");
			  
			 for(Element el:els){
				 listitemdata.clear();
				 remarks.clear(); 
				 int size = el.select("div[dir*=auto]").size();
				  
			    if(size>=3)
				  {
					 uberitem = new UberItem();
					 Elements moreelements = el.select("div[dir*=auto]");
						
					 for(Element data :moreelements) {
						  String records = data.text();
						  listitemdata.add(records);		
				     }
					
					 Integer itemquantity = (Integer) listitemdata.get(0);
					 String itemname = (String) listitemdata.get(1);
					 Integer itemprice = (Integer) listitemdata.get(2);
					 
					 uberitem.setItemname(itemname);
					 uberitem.setItemprice(itemprice);
					 uberitem.setItemquantity(itemquantity);					
				  }
				  
				else {
					  Elements more = el.select("div[dir*=auto]");
						
					  for(Element data :more) {
						  String mark = data.text();
						  System.out.println(mark);
						  remarks.add(mark);						
					  }
				
					  String remark = (String) remarks.get(0);		
					  uberorder.getUberitem().get(uberorder.getUberitem().lastIndexOf(uberitem)).setRemarks(remark);					
				   }
		              uberorder.setUberitem(Arrays.asList(uberitem));
				
			   }
			
				Elements membereprice = doc.select("div[dir*=auto]");

				for (Element elements : membereprice ) {

					String recordsdata = elements.text();
					listprice.add(recordsdata);

				}
		
				int size = listprice.size();
				
				for(int i=0;i<size;i++) {
					
					String DeliveryFee = (String) listprice.get(i);
					if(DeliveryFee.equals("Delivery Fee")) {
						index =i;
					
					}					
				}
				
				  Integer Subtotal = (Integer) listprice.get(index+1);
				  Integer Tax = (Integer) listprice.get(index+2);
				  Integer Total = (Integer) listprice.get(index+3);
				  Integer DeliveryFees = (Integer) listprice.get(index+4);
				  
				  uberorder.setSubtotal(Subtotal);
				  uberorder.setTax(Tax);
				  uberorder.setTotal(Total);
                  uberorder.setDeliveryFees(DeliveryFees);  
                  
                  uber = new Uber();
  				  details = new Details();
  				  
  			
  			      uber.setRefId(uberorder.getOrder_id());
                  details.setOrder_subtotal(uberorder.getSubtotal());
                  details.setChannel("Uber");
                  details.setTotal_charges(uberorder.getTotal());
                  details.setOrder_level_total_taxes(uberorder.getTax());
                  details.setDelivery_datetime(uberorder.getDeleverytime());
                  
                  Charge charge = new Charge();
         	     
          	      charge.setTitle("Delivery Charge");
          	      charge.setValue(uberorder.getDeliveryFees());	
          	      
          	      details.setCharges(Arrays.asList(charge));            
                  uber.setDetails(details);
                         	 	
                 for(UberItem items:uberorder.getUberitem()) {
                	 itemdata = new Item();
                	 itemdata.setQuantity(uberitem.getItemquantity());
                	 itemdata.setPrice(uberitem.getItemprice());
                     itemdata.setFood_type(uberitem.getRemarks());
                	 uber.setItems(Arrays.asList(itemdata));
                  }
                  
		      } catch (Exception e) {			
			    e.printStackTrace();
		 }
	}
	
	  public void deleverydata(String pageSource) {
		try {
			Document doc = Jsoup.parse(pageSource);
	
			Elements memberelements = doc.select("div[class*=rn-1oszu61 rn-1cp9tfb rn-1iymjk7 rn-s2skl2 rn-l5bh9y rn-101sy47 rn-1efd50x rn-14skgim rn-rull8r rn-mm0ijv rn-13yce4e rn-fnigne rn-ndvcnb rn-gxnn5r rn-deolkf rn-6koalj rn-1pxmb3b rn-7vfszb rn-eqz5dr rn-61z16t rn-p1pxzi rn-11wrixw rn-1wzrnnt rn-ifefl9 rn-bcqeeo rn-wk8lta rn-9aemit rn-1mdbw0j rn-gy4na3 rn-bnwqim rn-1lgpqti]");
			int size= memberelements.size();
			
			for(int i=0;i<size;i++) {
			
			for (Element elements : memberelements) {
				delevery  =new Delevery();
		
			   Elements member = elements.select("div[dir*=auto]");
			 
				 for(Element elments:member) {
					 String data = elments.text();
					 deleveryboy.add(data);		
	           }
				 
	   try {
				 
			UberThread.driver.findElements(By.xpath(
							"//div[@class='rn-1oszu61 rn-1cp9tfb rn-1iymjk7 rn-s2skl2 rn-l5bh9y rn-101sy47 rn-1efd50x rn-14skgim rn-rull8r rn-mm0ijv rn-13yce4e rn-fnigne rn-ndvcnb rn-gxnn5r rn-deolkf rn-6koalj rn-1pxmb3b rn-7vfszb rn-eqz5dr rn-61z16t rn-p1pxzi rn-11wrixw rn-1wzrnnt rn-ifefl9 rn-bcqeeo rn-wk8lta rn-9aemit rn-1mdbw0j rn-gy4na3 rn-bnwqim rn-1lgpqti']")).get(i).click();
			// after click we want also close tab
			  Elements data = elements.select("div[dir*=auto]");
			  for(Element number:data) {
				  
			   String no = number.text();
				  // status				
			 }		
		 }
			catch(Exception e) {
			 e.printStackTrace();			 
		    }
	   	   		
			String deleveryboy_name = (String) deleveryboy.get(0);
			String order_id= (String) deleveryboy.get(2);

			}
		  }		
		}
		catch(Exception e) {
			e.printStackTrace();
			
		}

	}

	
}
