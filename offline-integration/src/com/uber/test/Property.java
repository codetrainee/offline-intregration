package com.uber.test;

import java.io.FileInputStream;
import java.util.Properties;



public class Property {
	static String user;
	static String pass;
	
	
	public static Properties loadPropertiesFile() {
		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			String path = UtilsCommon.getJarContainingFolder(Property.class);
			
			fis = new FileInputStream(path+"/db.Properties");
			prop.load(fis);
			
	     user = prop.getProperty("user");
	     pass = prop.getProperty("password");
	
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		

		return prop;
	}
	


}
