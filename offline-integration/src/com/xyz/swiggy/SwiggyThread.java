package com.xyz.swiggy;

import java.io.BufferedReader;

import java.io.File;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import com.nt.model.Address;
import com.nt.model.Charge;
import com.nt.model.Customer;
import com.nt.model.Details;
import com.nt.model.Discount;
import com.nt.model.Extra_platform;
import com.nt.model.Item;
import com.nt.model.ItemCharge;
import com.nt.model.ItemTaxes;
import com.nt.model.OptionToAdd;
import com.nt.model.Payment;
import com.nt.model.Restaurant;
import com.nt.model.SourceMessageIdMap;
import com.nt.model.Store;
import com.nt.model.Swiggy;
import com.nt.model.Uber;
import com.nt.model.swiggy.Cart;
import com.nt.model.swiggy.Charges;
import com.nt.model.swiggy.ConfirmOrder;
import com.nt.model.swiggy.LoginSuccess;
import com.nt.model.swiggy.Orders;
import com.nt.model.swiggy.Outlets;
import com.nt.model.swiggy.RestaurantData;
import com.nt.model.swiggy.Status;
import com.nt.model.swiggy.SwiggyItem;
import com.nt.model.swiggy.SwiggyOrder;
import com.uber.test.Property;
import org.codehaus.jackson.map.DeserializationConfig;

public class SwiggyThread implements Runnable {

	static final String COOKIES_HEADER = "Set-Cookie";
	HttpURLConnection postConnection;

	List<String> cookiesHeader;

	static java.net.CookieManager msCookieManager = new java.net.CookieManager();

	private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// 2019-07-05T18:08:06
	static String status;
	static Integer user_id;
	static String responsedata;
	static long rest_rid;
	static String finaldate;
	public static final Date date = null;	
	static Swiggy swiggy;
	static Uber uber;	
	Details details = null;
	Charge charge = null;	
	Item itemdata =null;
	

	@Override
	public void run() {

		try {
			
			Login();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    }

	public void Login() throws IOException, InterruptedException {

		String user = Property.swigyuser;
		String pass = Property.swigypass;

		final String POST_PARAMS = "{\"username\":\"" + user + "\",\"password\":\"" + pass + "\"}";

		URL obj = new URL("https://partner.swiggy.com/authentication/v1/login/");

		postConnection = (HttpURLConnection) obj.openConnection();
		postConnection.setRequestMethod("POST");
		postConnection.setRequestProperty("Content-Type", "application/json");

		postConnection.setDoOutput(true);
		postConnection.setConnectTimeout(10000);

		OutputStream os = postConnection.getOutputStream();
		os.write(POST_PARAMS.getBytes());
		os.flush();
		os.close();

		int responseCode = postConnection.getResponseCode();
		System.out.println("POST Response Code :  " + responseCode);
		System.out.println("POST Response Message : " + postConnection.getResponseMessage());

		if (responseCode == 200) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			responsedata = response.toString();
			System.out.println(response.toString());
		} else {
			System.out.println("POST NOT WORKED");
		}

		Map<String, List<String>> headerFields = postConnection.getHeaderFields();
		cookiesHeader = headerFields.get(COOKIES_HEADER);

		if (cookiesHeader != null) {
			for (String cookie : cookiesHeader) {
				System.out.println(cookie);
				msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
			}
		}
	
		 LoginSuccess login = null;
		
		 ObjectMapper mapper2 = new ObjectMapper().setVisibility(JsonMethod.FIELD,
                 Visibility.ANY);
         mapper2.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
             false);		      
         try
             {
        	 login =  mapper2.readValue(responsedata, LoginSuccess.class);

            } catch (JsonGenerationException e)
                  {
                    e.printStackTrace();
                  }
                    catch (JsonMappingException e)
                  {
                   e.printStackTrace();
                  }
                    catch (IOException e)
                  {
                    e.printStackTrace();
                  }
         
         
           status = login.getStatusMessage();
           user_id = login.getUser_id();
         
         for(Outlets outlet :login.getOutlets()) {
        	 
        	 rest_rid = outlet.getRest_rid();
         }
  
		if (status.equals("Login Successful")) {

			FatchingData();
			
//			 markready();
//		     confirmorder();

		}

	}


	public void FatchingData() throws IOException, InterruptedException {

		Date date = new Date();

		String strDateFormat = "yyyy-MM-dd";
		DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
		String formattedDate = dateFormat.format(date);
		String year = formattedDate;

		String strDateFormat2 = "HH:mm:ss";
		DateFormat dateFormat2 = new SimpleDateFormat(strDateFormat2);
		String formattedDate2 = dateFormat2.format(date);
		String minute = formattedDate2;

		finaldate = year + "T" + minute;
		System.out.println(finaldate);

		swiggy = new Swiggy();
		Restaurant restaurant = new Restaurant();	
		restaurant.setRest_rid(rest_rid);
		restaurant.setLastUpdatedTime(finaldate);
		swiggy.setRestaurantTimeMap(Arrays.asList(restaurant));
		SourceMessageIdMap source = new SourceMessageIdMap();
		source.setSource("POLLING_SERVICE");
		swiggy.setSourceMessageIdMap(source);
		ObjectMapper mapperstatus = new ObjectMapper();
		String jsonStr = mapperstatus.writeValueAsString(swiggy);
		URL objdata = new URL("https://partner.swiggy.com/orders/v1/fetch");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		postConnection = (HttpURLConnection) objdata.openConnection();
		postConnection.setRequestMethod("POST");
		postConnection.setRequestProperty("Content-Type", "application/json");
		postConnection.setDoOutput(true);
		postConnection.setConnectTimeout(30000);
		postConnection.setReadTimeout(10000);

		if (msCookieManager.getCookieStore().getCookies().size() > 0) {

			for (String cookie : cookiesHeader) {
				postConnection.addRequestProperty("Cookie", cookie.split(";", 2)[0]);

			}
		}

		OutputStream os = postConnection.getOutputStream();
		os.write(jsonStr.getBytes());
		os.flush();
		os.close();

		int responseCode2 = postConnection.getResponseCode();
		System.out.println("POST Response Code :  " + responseCode2);
		System.out.println("POST Response Message : " + postConnection.getResponseMessage());

		if (responseCode2 == 200) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
			String inputLine2;
			StringBuffer response2 = new StringBuffer();

			while ((inputLine2 = in.readLine()) != null) {
				response2.append(inputLine2);
			}
			in.close();
			
			responsedata = response2.toString();
			System.out.println(response2.toString());
		} else {
			System.out.println("POST NOT WORKED");
		}
		
			
		try {

			    SwiggyOrder order2 = null;
		//	    ObjectMapper mapper3 = new ObjectMapper();
			    ObjectMapper mapper3 = new ObjectMapper().setVisibility(JsonMethod.FIELD,
			                         Visibility.ANY);
			    mapper3.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
			                     false);		
			                          
		      try
		      {
		     //	  order2 =  mapper3.readValue(responsedata, SwiggyOrder.class);
		    	  order2 =  mapper3.readValue(new File("/home/zeb/item.json"), SwiggyOrder.class);
		    	  
		      } catch (Exception e)
		      {
		         e.printStackTrace();
		      } 
     
		        uber = new Uber();
				details = new Details();
				charge = new Charge();
												
		      for(RestaurantData data :order2.getRestaurantData()) {
    	  
		    	  if(data.getOrders().isEmpty()){
		    		  
		    	  System.out.println("order object empty");
		      
		       }
		    	 else {
		    		  System.out.println("order object not empty");
		    		  
		    	     for(Orders order: data.getOrders()) { 
			    		  order.getOrder_id();			    		  
			    		  details.setOrder_subtotal(order.getBill());
			    		  details.setTotal_external_discount(order.getDiscount());
			    		  details.setChannel("swiggy");
			    		 			    		  
			    		  uber.setRefId(Long.parseLong(order.getOrder_id()));
			    		
			    		  Status  status = order.getStatus();
			    		
			    		  details.setDelivery_datetime(status.getOrdered_time());
			    		    
			    		  uber.setCurrentState(status.getOrder_status());
			    		  	  
			    		  uber.setDetails(details);
			 
			    		  Cart cart = order.getCart();
			    		  
			    		  Charges charges  = cart.getCharges();
			    		  System.out.println(cart.getItems());
			 
			    	   for(SwiggyItem item:cart.getItems()) {
			    		
			    	      itemdata = new Item();
			    			  
			    		  itemdata.setFood_type(item.getQuantity().toString());
			    		  itemdata.setTitle(item.getName());
			    		  itemdata.setTotal(item.getTotal());
			    		  itemdata.setQuantity(item.getQuantity());
			    			
			    		  uber.setItems(Arrays.asList(itemdata));
			    		
			    		 }  
			    	  }
		    	  } 
		      }
		      
		      for(RestaurantData restdata :order2.getRestaurantData()) {
		    	  
		    	  if(!restdata.getOrders().isEmpty()){
		    		  
		    		  postdata();		    	
		    	  }
		    	  
		   }
		  } catch (Exception e) {
			e.printStackTrace();
		}
		
		confirmorder();		
		markready();	
		
		FatchingData();	
		
		Thread.sleep(30000);
	 
	}
	
	
	public void confirmorder() {
		
		ConfirmOrder confirmorder = new ConfirmOrder();
		
	
//		String url = "https://partner.swiggy.com/orders/v1/confirmOrder/";
//		final String POST_PARAMS = "{\"order_id\": \"45411826484\",\"restaurantId\": 116910,\"prep_time\": 15}";
		
		try {
			 ObjectMapper mapper = new ObjectMapper();
			 String jsonStr = mapper.writeValueAsString(confirmorder); 
	         URL obj = new URL("https://partner.swiggy.com/orders/v1/confirmOrder/");
			    
			    HttpURLConnection postConn = (HttpURLConnection) obj.openConnection();
			  
			    postConn.setRequestMethod("POST");
			    postConn.setRequestProperty("Content-Type", "application/json");
			    
			    postConn.setDoOutput(true);
			    postConn.setConnectTimeout(10000);
			    postConn.setAllowUserInteraction(true);
			    
			    
			    OutputStream os = postConn.getOutputStream();
			    os.write(jsonStr.getBytes());
			    os.flush();
			    os.close();
			    int responseCode = postConn.getResponseCode();
			    System.out.println("POST Response Code :  " + responseCode);
			    System.out.println("POST Response Message : " + postConn.getResponseMessage());
			    
			    if (responseCode == 200) { //success
			        BufferedReader in = new BufferedReader(new InputStreamReader(
			        		postConn.getInputStream()));
			        String inputLine;
			        StringBuffer response = new StringBuffer();
			        
			        while ((inputLine = in .readLine()) != null) {
			            response.append(inputLine);
			        } in .close();
			       
			         responsedata = response.toString();     
			         System.out.println(responsedata);
			  
			    } else {
			        System.out.println("POST NOT WORKED");
			    }
			}
			catch(Exception e) {
				e.printStackTrace();
				
			}
		
		
		

	}

	public void markready() throws IOException {
		
		int rest_id = 116910;
		String order_id = "45411826484";
		String mark = "/mark-ready";

		final String POST_PARAMS = "{\"restaurantId\":\"" + rest_id + "\"}";	
		
		// https://partner.swiggy.com/api/order/45411826484/mark-ready
		
		String url = "https://partner.swiggy.com/api/order/"+order_id+mark;
		URL obj = new URL(url);

		postConnection = (HttpURLConnection) obj.openConnection();
		postConnection.setRequestMethod("POST");
		postConnection.setRequestProperty("Content-Type", "application/json");

		postConnection.setDoOutput(true);
		postConnection.setConnectTimeout(10000);

		OutputStream os = postConnection.getOutputStream();
		os.write(POST_PARAMS.getBytes());
		os.flush();
		os.close();

		int responseCode = postConnection.getResponseCode();
		System.out.println("POST Response Code :  " + responseCode);
		System.out.println("POST Response Message : " + postConnection.getResponseMessage());

		if (responseCode == 200) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			responsedata = response.toString();
			System.out.println(response.toString());
		} else {
			System.out.println("POST NOT WORKED");
		}
	}
	
	 public  void postdata() throws Exception {
		
		doTrustToCertificates();
		
		try {
		 ObjectMapper mapper = new ObjectMapper();
		 String jsonStr = mapper.writeValueAsString(uber); 
         URL obj = new URL("https://192.168.0.126:8080/ere4u-third-party/order/");
		    
		    HttpURLConnection postConn = (HttpURLConnection) obj.openConnection();
		  
		    postConn.setRequestMethod("POST");
		    postConn.setRequestProperty("Content-Type", "application/json");
		    
		    postConn.setDoOutput(true);
		    postConn.setConnectTimeout(10000);
		    postConn.setAllowUserInteraction(true);
		    
		    
		    OutputStream os = postConn.getOutputStream();
		    os.write(jsonStr.getBytes());
		    os.flush();
		    os.close();
		    int responseCode = postConn.getResponseCode();
		    System.out.println("POST Response Code :  " + responseCode);
		    System.out.println("POST Response Message : " + postConn.getResponseMessage());
		    
		    if (responseCode == 200) { //success
		        BufferedReader in = new BufferedReader(new InputStreamReader(
		        		postConn.getInputStream()));
		        String inputLine;
		        StringBuffer response = new StringBuffer();
		        
		        while ((inputLine = in .readLine()) != null) {
		            response.append(inputLine);
		        } in .close();
		       
		         responsedata = response.toString();     
		         System.out.println(responsedata);
		  
		    } else {
		        System.out.println("POST NOT WORKED");
		    }
		}
		catch(Exception e) {
			e.printStackTrace();
			
		}
	
	}
	
	@SuppressWarnings("restriction")
	public static void doTrustToCertificates() throws Exception {
	        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
	        TrustManager[] trustAllCerts = new TrustManager[]{
	                new X509TrustManager() {
	                    public X509Certificate[] getAcceptedIssuers() {
	                        return null;
	                    }

	                    public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
	                        return;
	                    }

	                    public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
	                        return;
	                    }
	                }
	        };

	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	        HostnameVerifier hv = new HostnameVerifier() {
	            public boolean verify(String urlHostName, SSLSession session) {
	                if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
	                    System.out.println("Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
	                }
	                return true;
	            }
	        };
	        HttpsURLConnection.setDefaultHostnameVerifier(hv);
	    }


}
